function callPopup() {
  var $link = $('.js-call-popup');
  var $close = $('.js-close-popup');
  if($link.length) {
    $link.on('click', function() {
      var href = $(this).attr('href');
      $.fancybox({
      	wrapCSS: 'custom-popup',
        href: href,
        openEffect  : 'none',
        closeEffect : 'none',
        padding: ['0', '0', '0', '0']
      });
    });

    $close.on('click', function(e) {
    	e.preventDefault();
    	$.fancybox.close();
    })
  }
}

function toggleDropTable() {
  var $link = $('.js-toggle-drop-table');
  if($link.length) {
    $link.on('click', function(e) {
      var $tr = $(this).closest('tr'),
          $drop = $tr.next('.row-drop');
      $tr.toggleClass('drop-open');
      $drop.toggleClass('visible');
      e.preventDefault();
    })
  }
}

function toggleChoose() {
  var $link = $('.js-toggle-choose'),
      $drop = $('.choose__drop');
  if($link.length) {
    $link.on('click', function(e) {
      $(this).toggleClass('active');
      $drop.toggleClass('visible');
      e.preventDefault();
    })
  }
}

function higlightRow() {
  var $table = $('.workers-table'),
      $row = $table.find('tbody tr').not('.row-controls');

  if($row.length) {
    $row.on('click', function(e) {
      var isCheckbox = $(e.target).is('label');
      var $checkbox = $(this).find('input[type=checkbox]');
      var isChecked = $checkbox.is(':checked');

      if(!isChecked) {
        $(this).addClass('row-checked')
      } else {
        $(this).removeClass('row-checked')
      }

      if(!isCheckbox) {
        $checkbox.prop("checked", !isChecked);
      } else {
        $checkbox.prop("checked", !isChecked);
        return false;
      }
    });
  }
}

function retinaSrc(){
 if (window.devicePixelRatio == 2) {
  var images = $('img.retina');
  for(var i = 0; i < images.length; i++) {
    var imageType = images[i].src.substr(-4);
    var imageName = images[i].src.substr(0, images[i].src.length - 4);
    imageName += "@2x" + imageType;
    images[i].src = imageName;
  }
 }
}

function placeholder(objInputs){
  if (objInputs.length) objInputs.placeholder();      
};

$(document).on('ready', function(){
  placeholder($('input[placeholder], textarea[placeholder]'));
	callPopup();
  toggleDropTable();
  toggleChoose();
  higlightRow();
  retinaSrc();
});