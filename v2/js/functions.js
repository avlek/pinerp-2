function placeholder(objInputs){
  if (objInputs.length) objInputs.placeholder();      
};

function initCustomSelect($obj) {
	var selectConfig = {	
		header: false,
		height: 'auto',
		minWidth: 'auto',	
		classes: 'select',
		noneSelectedText: 'select options',
		selectedList: 1,
		multiple: false,
		position: {
			my: 'left top',
			at: 'left bottom',
			collision: 'flip flip',
			using: function(position, feedback) {				
				addPositionClass(position, feedback, $(this));
			}
		},
		arrow: true,
		divider: false,
		corner: false,			
		icon: false,
		jscrollpane: false,
		filter: false,
		filterOptions: {}	
	}
	if($obj.length) {
		customSelect($obj, selectConfig)
	}
}

function toggleControls() {
	var $link = $('.js-toggle-controls'),
			$controls = $('.controls');

	if($link.length) {
		$link.on('click', function(e) {
			var $drop = $(this).next('.controls');
			$drop.slideToggle(150);
			e.preventDefault();
		});

		$controls.on('mouseleave', function() {
			$(this).slideUp(200);
		});
	}
}

function toggleSettings() {
	var $link = $('.js-toggle-settings'),
			open = false;

	if($link.length) {
		$link.on('click', function(e) {
			e.preventDefault();
			$drop = $(this).next('.settings__tooltip');
			$drop.toggleClass('active');
			open = !open;
		});

		$(document).on('click', function(e) {
			if(open) {
				var isBtn = !!$(e.target).closest('.js-toggle-settings').length;
				var isDrop = !!$(e.target).closest('.settings__tooltip').length;

				if(!isBtn && !isDrop) {
					$drop.removeClass('active');
					open = false;
				}
			}
		});
	}
}

function retinaSrc(){
 if (window.devicePixelRatio == 2) {
  var images = $('img.retina');
  for(var i = 0; i < images.length; i++) {
	  var imageType = images[i].src.substr(-4);
	  var imageName = images[i].src.substr(0, images[i].src.length - 4);
	  imageName += "@2x" + imageType;
	  images[i].src = imageName;
  }
 }
}

function navLine() {
	var $el, leftPos, newWidth,
	    $mainNav = $(".nav-tabs");

  if($mainNav.length) {
    $mainNav.append("<li id='magic-line'></li>");
    var $magicLine = $("#magic-line");
    
    $magicLine
        .width($(".nav-tabs .active").width())
        .css("left", $(".nav-tabs .active a").position().left)
        .data("origLeft", $magicLine.position().left)
        .data("origWidth", $magicLine.width());
        
    $(".nav-tabs li").hover(function() {
        $el = $(this);
        leftPos = $el.position().left + 15;
        newWidth = $el.find('a').width();
        $magicLine.stop().animate({
            left: leftPos,
            width: newWidth
        });
    }, function() {
        $magicLine.stop().animate({
            left: $magicLine.data("origLeft"),
            width: $magicLine.data("origWidth")
        });    
    });
  }
}

function pseudoHref() {
	var $pseudo = $('[data-href]');
	if ( $pseudo ) {
		$pseudo.on('click',function(e) {
			window.location.href = $(this).attr('data-href');
		});
	}
}

function validator($form, options) {
	var $form = $form;
	if ($form.length) {
		$form.validate({
			rules: options.rules,
			messages: options.messages,
			errorClass: 'error',
			validClass: 'valid',
			submitHandler: function(form, e) {
				e.preventDefault();
				
				// serialize
				var seria = $form.serialize();

				$.ajax({
					url: $form.attr('action'),
					dataType: 'json',
					data: seria,
					success: function(data){
						if(data['status'] == "true"){
							alert('Форма успешно отправлена');
							$form.get(0).reset();
						}
					}      
				});
			},

			errorPlacement: function (error, element) {
				if (element.attr("type") == "checkbox" || element.attr("type") == "radio") {
				} else {
					element.after(error)
				}
			}  
		});
	}
}


$(document).on('ready', function(){
	placeholder($('input[placeholder], textarea[placeholder]'));
	initCustomSelect($('select'));
	toggleControls();
	toggleSettings();
	retinaSrc();
	navLine();
	pseudoHref();

	validator($('.login__form'), {
		rules: {
			login_email: { required: true, email: true },
			login_password: { required: true }
		}
	});
});